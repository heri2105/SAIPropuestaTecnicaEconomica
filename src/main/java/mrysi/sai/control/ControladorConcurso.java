/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.sai.control;
import java.util.List;
import mrysi.sai.entidades.Concurso;
import mrysi.sai.entidades.Proveedor;
import mrysi.sai.oad.OadConcurso;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/concurso")
public class ControladorConcurso {
     private final static Logger LOG = LoggerFactory.getLogger(ControladorConcurso.class);
     
    @Autowired
    OadConcurso oadConcurso;
    
    @GetMapping("")
    public List<Concurso> listaConcursos(){
        LOG.debug("GET");
        return oadConcurso.findAll();
    }
     @GetMapping("/{id}")
    public Concurso buscarProveedorConcurso(@PathVariable("id") Integer id){
        LOG.debug("GET");
        return oadConcurso.findOne(id);
    }
    
    @PutMapping("/{id}")
    public Concurso actualizarFallo(@PathVariable("id") Integer id, @RequestBody Concurso con) {
        LOG.debug("PUT {}", con);
        con.setIdConcurso(id);
        oadConcurso.save(con);
        return con;
    }
}
