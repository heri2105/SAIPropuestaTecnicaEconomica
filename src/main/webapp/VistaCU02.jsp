<%-- 
    Document   : VistaCU02
    Created on : 31/07/2018, 10:36:42 PM
    Author     : Heriberto
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head ng-app="AppCU02">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Consultar Propuesta Tecnica</title>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.6/angular.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="js/VistaCU02.js"></script>
        <script>
            $(document).ready(function () {
                $("#ert").click(function () {
                    console.log("aaaa");
                    alert("Actualizado Correctamente");
                });
            });

        </script>  
    </head>
    <body>

        <div ng-controller="AppCU02">
            <div class="row">
                <div class="col-lg-12"><center><h4>CU02 Consultar Propuesta Tecnica</h4></center></div>
            </div><br>
            <div class="row">
                <div class="col-lg-2"></div> 
                <div class="col-lg-10"> <strong>Participante:</strong>  Contructora Xalapa S.A de C.V</div> 
            </div>
            <div>
                <div class="row">
                    <div class="col-lg-2"></div> 
                    <div class="col-lg-10">
                        <object data="./PT/formato_propuesta_tecnica.pdf" width="800" height="600" type="application/pdf">
                            alt : <a href="./PT/formato_propuesta_tecnica.pdf">Archivo.pdf</a>
                        </object> 
                    </div> 
                </div>
                <div class="row">
                    <div class="col-lg-2"></div> 
                    <div class="col-lg-2">
                        <div class="radio">
                            <label><input type="radio" name="optradio">Solvente</label>
                        </div> 
                    </div>
                    <div class="col-lg-2">
                        <div class="radio">
                            <label><input type="radio" name="optradio" checked>No Solvente</label>
                        </div> 
                    </div>

                </div>
                <div class="row">
                    <div class="col-lg-2"></div> 
                    <div class="col-lg-2">
                        <strong>Observaciones</strong>
                    </div>
                    <div class="col-lg-4">
                        <textarea class="form-control" rows="5" id="comment" ng-model="propuesta.paginas"></textarea>
                        
                      
                    </div>
                </div><br> 
                <div class="row">
                    <div class="col-lg-6"></div> 
                    <div class="col-lg-1">
                        <button type="button" class="btn btn-primary">Cerrar </button>
                    </div>
                    <div class="col-lg-1">
                          <button type="button" class="btn btn-primary" id="ert">Actualizar</button>  
                      <!--  <input id="actualizar" type="button" value="Actualizar" ng-click="actConcurso(propuesta.idconcurso)" class="btn btn-primary" id="ert">  -->
                    </div>
                </div>

            </div>





            <br><br><br>
        </div> 

    </body>
</html>
