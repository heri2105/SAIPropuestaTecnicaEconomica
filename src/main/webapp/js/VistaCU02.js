var apppropuesta = angular.module('AppCU02', []);

apppropuesta.controller('AppCU02', ['$scope', '$http', '$log', '$location',
    function ($scope, $http, $log, $location) {
        $log.debug('definiendo controlador');



        //comienza a guardar datos del libro

        $scope.proveedor = [];
        $scope.cargarConcurso = function () {
            $http.get('concurso')
                    .then(function (respuesta) {
                        $log.debug(respuesta.data);
                        $scope.proveedor = respuesta.data;
                    });
        };


        //
        $scope.propuesta = {};
        $scope.epropuesta = function (idconcurso) {
            $scope.propuesta.id = idconcurso;
            $http.get('concurso/' + $scope.propuesta.id)
                    .then(function (respuesta) {
                        $log.debug(respuesta.data);
                        $scope.propuesta = respuesta.data;


                    });
        };
        //metodo para actualizar libro existente


        $scope.actConcurso = function (idconcurso) {
            $scope.propuesta.id = idconcurso;
            $log.debug("funcion actualizar propuesta");
            $http.put('concurso/' + $scope.propuesta.id, $scope.propuesta)
                    .then(function (respuesta) {
                        $log.debug(respuesta);
                    },
                            function (respuesta) {
                                $log.debug("POST error");
                            })
        };





        $scope.cargarConcurso();
    }
]);